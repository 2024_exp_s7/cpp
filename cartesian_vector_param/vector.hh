#ifndef vector_hpp
#define vector_hpp

#pragma once

#include <ostream>

#include <initializer_list>

#include "config.h"

class Vector
{
public:
// DO NOT CHANGE THIS
    Vector(const Vector&) = default;
    Vector& operator=(const Vector&) = default;
    ~Vector() = default;
//

// Add suitable constructors
    Vector();
    Vector(std::initializer_list<int> list);
    // possibly more

// Public Member functions here
    Vector& operator+=(const Vector& rhs);
    Vector& operator+=(int val);

    Vector& operator-=(const Vector& rhs);
        Vector& operator*=(const Vector& rhs);
        Vector& operator*=(const int& val);

        Vector& operator+(const Vector& rhs);
        Vector& operator-(const Vector& rhs);

        int& operator*(const Vector& rhs);

        int& operator[](int rhs);

    // More to go

private:
// Private Member functions here (if necessary)
    value data[NDIM];
    size_t vector_size;
// Member variables are ALWAYS private, and they go here
};

// Nonmember function operators go here
Vector operator*(const value& s, const Vector& v);
Vector operator+(const value& s, const Vector& v);
std::ostream& operator<<(std::ostream& o, const Vector& v);
#endif